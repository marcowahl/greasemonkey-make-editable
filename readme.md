`make editable` makes webpages editable.

# Usage #

With script `make editable` enabled any webpage gets an extra button
in the left upper corner.  A click on the button unleashes editing
operations for the page.

The indirection with the button is to let the user explicitly decide
for each page when to activate `make editable`.

Note that some typical page functionality breaks after the click,
e.g. following links.

Reload the page to restore the original content.

# Why? #

 - Fun
 - Get rid of annoying parts temporarily

# Repo #

https://gitlab.com/marcowahl/greasemonkey-make-editable

# Example #

![img](screenshot-20161208-150352.png)
